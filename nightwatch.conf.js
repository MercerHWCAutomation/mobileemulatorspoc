var seleniumServer = require('selenium-server');
var chromedriver = require('chromedriver');
var firefoxdriver=require('geckodriver');
//var ieDriver=require('iedriver');
var data = require('./TestResources/GlobalTestData');
sauce_labs_username = "subhajit-chakraborty";
sauce_labs_access_key = "8f7f091b-9858-4f6a-8585-f4ceeb682511";

require('nightwatch-cucumber')({
    supportFiles: ['./utils/TestExecListener.js'],
    stepTimeout:900000,
    defaultTimeoutInterval:900000
})

module.exports = {
  output_folder: 'reports',
  custom_commands_path: '',
  custom_assertions_path: '',
  page_objects_path : "repository",
  test_workers: {
      enabled: false,
      workers: 3
  },
  live_output: false,
  disable_colors: false,
  //test_workers : false,
  selenium: {
    start_process: true,
    server_path: seleniumServer.path,
    host: '127.0.0.1',
      port: 5555,
    cli_args: {
        'webdrover.chrome.driver': chromedriver.path,
        'webdriver.ie.driver': 'C:\\Users\\subhajit-chakraborty\\Documents\\IEDriverServer.exe',
        'webdriver.firefox.profile': 'MMC_USER_52',
        'webdriver.gecko.driver' : 'C:\\Health_Automation\\node_modules\\geckodriver\\geckodriver.exe'
    }
  },

  //test_settings: {
    //  default: {
    //      launch_url: "http://localhost",
    //      page_objects_path: "repository",
    //      selenium_host: "127.0.0.1",
    //      selenium_port: 5555,
    //      silent: true,
    //      disable_colors: false,
    //      screenshots: {
    //          enabled: true,
    //          on_failure: true,
    //          on_error: true,
    //          path: 'screenshots'
    //      },
//
    //      desiredCapabilities: {
    //          browserName: "chrome",        //chrome,firefox,internet explorer
    //          javascriptEnabled: true,
    //          acceptSslCerts: true,
    //      },
    //  },
  //}
//---------------------------------------------------------------------
     "test_settings": {
         default: {
             launch_url: 'http://ondemand.saucelabs.com:80',
             selenium_port: 80,
             page_objects_path: "repository",
             selenium_host: 'ondemand.saucelabs.com',
             silent: true,
             username: 'subhajit-chakraborty',
             access_key: '8f7f091b-9858-4f6a-8585-f4ceeb682511',
             screenshots: {
                 enabled: false,
                 on_failure: true,
                 on_error: true,
                 path: './screenshots'
             },
             globals: {
                 waitForConditionTimeout: 10000
             },
             desiredCapabilities: {
                 name: 'DemoTest',
                 browserName: 'firefox',
                 parentTunnel: 'erik-horrell',
                 tunnelIdentifier: 'MercerTunnel1',
             }
         },
         chrome: {
             desiredCapabilities: {
                 browserName: 'chrome',
                 javascriptEnabled: true,
                 acceptSslCerts: true,
                 chromeOptions: {
                     prefs: {
                         "credentials_enable_service": false,
                         "profile.password_manager_enabled": false
                     }
                 },
                 platform: "Windows 10",
                 version: "63"
             },

         },
         "firefox": {
             "desiredCapabilities": {
                 browserName: 'firefox',
                 javascriptEnabled: true,
                 acceptSslCerts: true,
                 platform: "Windows 7",
                 version: "58"
             }
         },
         "internet_explorer_11": {
             "desiredCapabilities": {
                 "platform": "Windows 7",
                 "browserName": "internet explorer",
                 "version": "11"
             }
         },
         "IEEdge": {
             "desiredCapabilities": {
                 "platform": "Windows 10",
                 "browserName": "microsoftedge",
                 "version": "11",
                 "javascriptEnabled": "true",
                 "acceptSslCerts": "true",
                 "device": "",
             }
         },
         "android_s4_emulator": {
             "desiredCapabilities": {
                 "browserName": "android",      //android
                 "deviceOrientation": "portrait",
                 "deviceName": "Samsung Galaxy S4 Emulator"
             }
         },

         "android_7_emulator": {
             "desiredCapabilities": {
                 "browserName": "android",
                 "deviceOrientation": "portrait",
                 "deviceType":"phone",
                 "deviceName": "Android GoogleAPI Emulator",
                 "platformVersion":"7.1"
             }
         },

         "iphone_6_simulator": {
             "desiredCapabilities": {
                 "browserName": "iPhone",
                 "deviceOrientation": "portrait",
                 "deviceName": "iPhone 6",
                 "platform": "OSX 10.10",
                 "version": "8.4"
             }
         },

         "Samsung_Galaxy": {
             "desiredCapabilities": {
                 "browserName": "android",
                 "deviceOrientation": "portrait",
                 "deviceName": "Samsung Galaxy Nexus GoogleAPI Emulator",
                 //"platform": "",
                // "version": "",
             }
         }
     }

}

