/**
 * Created by subhajit-chakraborty on 5/16/2017.
 */

module.exports = {

    //Specify Your Testing Environment

    //TestingEnvironment: 'QAInt',
    TestingEnvironment: 'QASandbox',
    //TestingEnvironment: 'DEVCLIENTC',
    //TestingEnvironment: 'CI Test',


    shortWait: 5000,
    MedWait: 5000,
    longWait: 10000,
    LoginWait: 30000,
    CarrierDescLoadTime: 60000,


    //Environment User details
     AdminToolkitURL: 'http://usfkl13ws19v/webcf/',                     //QA Sandbox
     //AdminToolkitURL: 'http://usfkl13ws19v.mercer.com:91/webcf/',     //QA Int
    //AdminToolkitURL: 'http://usfkl13ws18v:83/webcf/',                 //Dev Client C
    //AdminToolkitURL: 'https://usba.mercer.com/webconfig/',            //CI Test

    //Content Ep

     //ClientNameContentEPAddRule: 'UHCMSP',
     ClientNameContentEPAddRule: 'QAACME',
     //ClientNameContentEPAddRule: 'MBC QA CLIENT THREE',
     //ClientNameContentEPAddRule: 'MBC QA CLIENT Five',
     ContentEPValue: 'Accounts Benefit',
     NewRuleName: 'TestRule123',
     NewRuleDescription: 'TestRule123',
     ExpressionNewRule: 'true',
     NewRuleBenSummary: 'I am new Rule',
     BenefitHeaderTextbox: 'I am Ben Header',
     BenefitContentTextbox: 'I am Benefit Context',

    //Hybrid EP Data:
     HybridEPValue: 'Supplemental Medical Derive Option Javascript',
     HybridEpUpdatedName: 'Rule Test QA1',

    //Javascript EP
    JavascriptEPValue:'Dependent Benefit Eligibility Javascript',
    JavascriptNewRuleName: 'TestRuleMission',
    JavascriptNewRuleDescription: 'Description 1',
    JavascriptExpressionNewRule: 'if(In(DependentEligData.RelationType, "CH", "FS", "CDP", "LD", "CSS", "DISCH", "DCDP", "DCSS","SP", "DP", "SGSP"), if(DependentEligData.DepAgeAsOfLifeEventDate < 26 or In(DependentEligData.RelationType, "DISCH", "DCDP", "DCSS") or In(DependentEligData.RelationType,"SP", "DP", "SGSP") or DependentEligData.MedicareEligibilityStatus="Y", if(InStr(DependentEligData.EmpCgData_Usercode_2, "ACT") > 0, if(DependentEligData.IsDepCoveredAsEmployee("MEDICAL") Or DependentEligData.IsDepCoveredAsEmployee("RETMEDU65") Or DependentEligData.IsDepCoveredAsEmployee("RETMEDO65") Or DependentEligData.IsDepCoveredAsEmployee("DENTAL") Or DependentEligData.IsDepCoveredAsEmployee("RETDENTAL") Or DependentEligData.IsDepCoveredAsEmployee("VISION") Or DependentEligData.IsDepCoveredAsEmployee("RETVISION"), "[]", "[MEDICAL,RX,DENTAL,VISION]" ), if(DependentEligData.IsDepCoveredAsEmployee("MEDICAL") Or DependentEligData.IsDepCoveredAsEmployee("RETMEDU65") Or DependentEligData.IsDepCoveredAsEmployee("RETMEDO65") Or DependentEligData.IsDepCoveredAsEmployee("DENTAL") Or DependentEligData.IsDepCoveredAsEmployee("RETDENTAL") Or DependentEligData.IsDepCoveredAsEmployee("VISION") Or DependentEligData.IsDepCoveredAsEmployee("RETVISION"), "[]", if(DependentEligData.DepAgeAsOfLifeEventDate >= 65 Or DependentEligData.MedicareEligibilityStatus="Y", "[RETMEDO65,RETRXO65,RETDENTAL,RETVISION,DENTAL,VISION]", "[RETMEDU65,RETRXU65,RETDENTAL,RETVISION,DENTAL,VISION]" ) ) ), "[]" ), "[]" )',

    //HB Client Selection
    Dynamic: 'N',
    Precalculated: 'Y',      //Type 'Y' and 'N' for your Selections
    RelativeFolderValue: 'AA',

    //Client Config Visibility
    VisibilityDateEnable: 'Y' ,  //Type 'Y' if you want to enable else 'N' for Disabling

    //Portal Data
    OETileDisplayOrderValue: '10',

    //Best Match Details
    ActuarialValue: '0.712',

    //Promote Page Details:

    ItemToPromoteName: 'Portal Settings',

    //BPO Details
    LongDescriptionText: '34'

}
