/**
 * Created by subhajit-chakraborty on 5/16/2017.
 */

module.exports = {
    sections: {
        ClientListPage: {
            selector: 'body',
            elements: {

                ClientListHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Client List')]"},
                BenefitsHubHeader: {locateStrategy: 'xpath',selector: "//span[contains(text(),'HUB')]"},
                FirstClientListName: {selector: "tbody tr:nth-child(2) td a"},
                FilterTabBenHubPage: {locateStrategy: 'xpath',selector: "//input[@id='client-list-filter']"},
            }
        },

        WebConfigurationPage: {
            selector: 'body',
            elements: {

                WebConfigHeader: {locateStrategy: 'xpath',selector: "//h2[contains(text(),'Web Configuration')]"},
                ContentEPValueFilter: {locateStrategy: 'xpath', selector: "//input[@id='eval-points-filter']"},
                EPValue1Description: {selector: "td.eval-point-description a"},

            }
        },

        EPRulesPage: {
            selector: 'body',
            elements: {

                RulesHeader: {locateStrategy: 'xpath',selector: "//h4[contains(text(),'Rules')]"},
                AddRuleButton: {locateStrategy: 'xpath',selector: "//a[contains(.,'Add Rule')]"},
                CreateNewRuleHeader: {locateStrategy: 'xpath',selector: "//h4[contains(text(),'Create New Rule')]"},
                NewRuleName: {selector: "#eval-point-rule-data-name"},
                NewRuleDescription: {selector: "#eval-point-rule-data-description"},
                RuleDescDeletePage: {selector: "tbody tr td.mul-word-break"},

                ExpressionTextarea: {selector: "pre[id='expression_editor']"},
                ExpressionTextAreaOperator: {selector: "span.ace_keyword.ace_operator"},
                ExpressionTextAreaValue: {locateStrategy: 'xpath',selector: "(//textarea[@class='ace_text-input'])[1]"},



                BenefitSummaryTextbox: {selector: "#BenefitSummary"},
                YourBenHeaderTextbox: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Your Benefit Header')]/../../input[@id='eval-point-content-title']"},
                BenefitContentTextbox: {selector: "#YourBenefitContent"},
                SpendingDDList: {locateStrategy: 'xpath',selector : "//label[contains(text(),'Drop Down List:')]/../select[@id='eval-point-content-select']"},
                HSAOptionSpendingDDList: {selector: "option[value='HSA']"},
                RetMedUOption: {selector: "option[value='RETMEDU65']"},
                RuleSaveButton: {selector: "div.col-24.mul-margin-bottom.js-eval-point-actions button[title='Save']"},
                ValidateSyntaxButton: {selector: "button[title='Validate Syntax']"},
                ConfirmationMsgSyntax: {locateStrategy: 'xpath',selector: "//h6[contains(text(),'Confirmation')]"},
                ConfirmationMsgRule: {locateStrategy: 'xpath',selector: "//p[contains(.,'and published the Evaluation Point')]"},
                ConfirmationDeleteRule: {locateStrategy: 'xpath',selector: "//h6[contains(text(),'Confirmation')]"},
                EditRule:{selector: "a[title='Edit Rule']"},
                EditNameHybridEdit: {selector: "input[title='Enter a rule name.']"},
                SaveButtonEditHybridRule: {locateStrategy: 'xpath',selector: "//a[@title='Cancel']/../button[@title='Save']"},
                ConfirmationEditedRule: {locateStrategy: 'xpath',selector: "//strong[contains(text(),'edited')]"},

                //Delete Rule
                FilterEPTextbox: {locateStrategy: 'xpath',selector: "//input[@id='eval-points-rules-filter']"},
                RuleNameSearch1: {selector: "tbody tr:nth-child(2) td strong"},
                DeleteRuleLink: {selector: "a[title='Delete Rule']"},
                DeleteTabConfirmation: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Delete')]"},

                //newly added
                SelectDropdownINT: {selector: "select[title='Select a choice.']"},
                AccidentOptionSDINT: {selector: "select[title='Select a choice.'] option[value='ACCIDENT']"},

            }
        },

        ClientSettingPage: {
            selector: 'body',
            elements: {

                ClientSettingLink: {locateStrategy: 'xpath',selector: "//li[3]/a[contains(text(),'Client Settings')]"},
                HBClientSettingLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'HB Client Settings')]"},
                DBClientSettingLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'DB Client Settings')]"},
                VisibilityDCConfigLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Visibility Date Client Configuration')]"},
                //PortalLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Portal')]"},

                //HB
                ClientSettingHeader: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Web Configuration - Client Settings')]"},
                DynamicRadioButton: {selector: "input[title='Dynamic']"},
                PreCalculateRadioButton: {selector: "input[title='Precalculate']"},
                SaveButtonClientSetting: {selector: "button[title='Save']"},
                CongratulationMsgHB: {locateStrategy: 'xpath',selector: "//p[contains(text(),'You have successfully saved HB Settings for')]"},

                //DB
                DBSettingHeader: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'DB Settings')]"},
                RelativeFolderTextbox: {locateStrategy: 'xpath',selector: "//label[contains(text(),'Relative Folder Path for PDF Worksheets:')]/../input"},
                SaveButtonDB: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Cancel')]/../button[@title='Save']"},
                CongratulationMsgDB: {locateStrategy: 'xpath',selector: "//p[contains(text(),'You have successfully saved DB Settings for')]"},

                //Visibility Date Client Configuration
                VisibiltyClientConfigLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Visibility Date Client Configuration')]"},
                VisibilityDateEnableRadioButton: {locateStrategy: 'xpath',selector: "//label[contains(.,'Enable')]/input"},
                VisibilityDateDisableRadioButton: {locateStrategy: 'xpath',selector: "//label[contains(.,'Disable')]/input"},
                VisibilitySaveButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Cancel')]/../button[@title='Save']"},
                ConfirmationMsgVisibilityConfig: {locateStrategy: 'xpath',selector: "//b[contains(text(),'Visibility Date')]"},

                //Portal Details
                PortalLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Portal')]"},
                OETileDisplayTextbox: {locateStrategy: 'xpath',selector: "//label[contains(text(),'Open Enrollment Tile display order')]/../input"},
                PortalSaveButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Cancel')]/../button[@title='Save']"},
                ConfirmationMsgPortal: {locateStrategy: 'xpath',selector: "//p[contains(text(),'You have successfully saved PORTAL Settings for ')]"},

                //new
                HideRadioButton: {selector: "input[title='Hide']"},








            }
        },

        BestMatchPage: {
            selector: 'body',
            elements: {

                BestMatchLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Best Match')]"},
                EditFirstBestMatch: {locateStrategy: 'xpath',selector: "//tbody/tr[2]/td/a[@title='Edit']"},
                ActuarialValue: {selector: "input[title='Enter an actuarial value.']"},
                SaveButtonBestMatch: {locateStrategy: 'xpath',selector: "//a[@title='Cancel']/../a[@title='Save']"},
                BestMatchConfirmationMsg: {locateStrategy: 'xpath',selector: "//p[contains(text(),'You have successfully saved Best Match for ')]"}


            }
        },

        PromotePage: {
            selector: 'body',
            elements: {
                PromoteLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Promote')]"},
                CITestToCSOPromote: {selector: "a[title='Promote CITEST to CSO']"},
            }
        },

        BPOCarrierDescPage: {
            selector: 'body',
            elements: {
                BPOCarrDescLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'BPO / Carrier Descriptions')]"},
                CarrierDescLink: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Carrier Descriptions')]"},
                CarrierDescHeader: {locateStrategy: 'xpath',selector :"//h2[contains(text(),'Web Configuration - Carrier Descriptions')]"},
                FirstLongDescTextbox: {selector: "tr:nth-child(1) #long-description"},
                SaveButtonBPO: {selector: "a[title='Save']"},
                ConfirmationMsgBPO: {locateStrategy: 'xpath',selector: "//p[contains(text(),'You have successfully updated ')]/strong[contains(text(),'Carrier Descriptions')]"},
                FirstEditOption: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Edit')]/../../../tr[1]/td[1]/a"},


            }
        },

        SamplePage: {
            selector: 'body',
            elements: {

                BenefitCentralHeader: {selector: "span.productname"},
                LoginToYourAccount: {selector: "span[title='Log In to Your Account']"},
                Username: {selector: "input[placeholder='User Name']"},
                Password: {selector: "input[placeholder='Password']"},
                Enter: {selector: "input[value='Enter']"},
                CurrentCoverage: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Current Benefits')]"},
                LogOutButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Log Out')]"},

                PopupContent: {selector: "div.popup_content"},
                ConfirmLogout: {selector: "input.button.primarybutton"},
                LogoutSuccess: {locateStrategy: 'xpath',selector: "//p[contains(text(),'You have been successfully logged out of your account.')]"},
            }
        },
    }
};
